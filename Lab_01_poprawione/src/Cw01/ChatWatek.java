package Cw01;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class ChatWatek extends Thread{
	private DatagramSocket DatagramSocket;

	
	//konstruktor
    public ChatWatek(DatagramSocket Socket) {
        this.DatagramSocket = Socket;
    }
    
    public void run() {
        try {     
            while (true) {
                byte[] buffer = new byte[1024];
		        DatagramPacket reply = new DatagramPacket(buffer , buffer.length );
		        DatagramSocket.receive(reply);
		        System.out.println( "Odpowied� serwera : " + new String(reply.getData()));
            }

        } catch (Exception e) {
            System.out.println("Wyjatek: " + e);
        }
    }
}
