package Cw01;

import javax.xml.crypto.Data;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class UDPServer {
	
	private DatagramSocket aSocket;
	private byte[] buffer = new byte [1024];
	
	private HashMap<String,User> users = new HashMap<String,User>();
	
	public UDPServer (DatagramSocket Socket) {
		this.aSocket=Socket;
	}
	
	public static void main ( String [] args ) throws SocketException {
		DatagramSocket aSocket = new DatagramSocket(9876);
		UDPServer server = new UDPServer(aSocket);
		server.ReciveAndSend();
	}
	
	
	public void ReciveAndSend () {
		
		try {
		 	System.out.println("Serwer ruszy� ma�y sukces!");
		 	while (true) {
		 		
		 		DatagramPacket DataPacket = new DatagramPacket(buffer, buffer.length);
		 		aSocket.receive(DataPacket);
		 		InetAddress Adres = DataPacket.getAddress();
		 		int port = DataPacket.getPort();
		 		//aSocket.receive(DataPacket);
		 		System.out.println(port + " | " +Adres +" | ");
				//przypisanie do zmiennej ReciveInfo otrzymanej wiadomosci od klienta
				String ReciveInfo = new String(DataPacket.getData(), StandardCharsets.UTF_8);
				System.out.println(" Wiadomosc od klienata " +ReciveInfo);
		 		//uzyskanie zanku do wykorzystania w case 
				String polecenie = ReciveInfo.split("|")[0];
				String nazwa = ReciveInfo.split("[|]")[1];
				
				switch (polecenie) {
					case "+":
						if(this.users.containsKey(nazwa)) {
							byte[] komunikat = ("Uzystownik o nazwie " + nazwa + " ju� istnieje!! Prosze zmieni� nazw�").getBytes();
							DatagramPacket packet = new DatagramPacket(komunikat, komunikat.length, Adres, port);
							aSocket.send(packet);
						}else {

							this.users.put(nazwa, new User(nazwa, DataPacket.getAddress(), DataPacket.getPort()));

							byte[] komunikat = ("Poleceni: + | " + nazwa + " | zosta�o wykonane - Uzytkownik dodany").getBytes();
							DatagramPacket packet = new DatagramPacket(komunikat, komunikat.length, Adres, port);
							aSocket.send(packet);
						}
						
						break;
					case "-":
				
						if (this.users.containsKey(nazwa)) {
							byte[] komunikat = ("Uzystownika o nazwie: " + nazwa+ " niema w bazie").getBytes();
							DatagramPacket packet = new DatagramPacket(komunikat, komunikat.length, Adres, port);
							aSocket.send(packet);
						} else {
							this.users.remove(nazwa);
							byte[] komunikat = ("polecenie: - | " + nazwa + " | zosta�o wykonane - uzytkownik usuni�ty").getBytes();
							DatagramPacket packet = new DatagramPacket(komunikat, komunikat.length, Adres, port);
							aSocket.send(packet);
						}

						break;
					case "?":
						List<String> SzukanieUzytkownikow = new ArrayList<String>();

						if (nazwa.equals(null)) {
							SzukanieUzytkownikow.addAll(this.users.keySet());
						} else {
							this.users.forEach((k,v) -> {
								if (k.matches(nazwa)) SzukanieUzytkownikow.add(k);
							});
						}
						
						byte[] komunikat = (String.join(",", SzukanieUzytkownikow)).getBytes();
						DatagramPacket packet = new DatagramPacket(komunikat, komunikat.length, Adres, port);
						aSocket.send(packet);
						break;
					case "!":
					    //0 - potwierdzenie dla nadawcy, 1 - wiadomosc dla odbiorcy
					
						String DoWyslania = ReciveInfo.split("[|]")[2];

						DatagramPacket potwierdzenia;
						DatagramPacket wiadomosc;

						if (!this.users.containsKey(nazwa)) {
							byte[] info = ("User with name " + nazwa + "doesn't exist").getBytes();
							potwierdzenia = new DatagramPacket(info, info.length, Adres, port);
							wiadomosc = null;
						} else {
							User odbiorca = this.users.get(nazwa);
							byte[] info = ("! | " + nazwa + " | OK").getBytes();
							potwierdzenia = new DatagramPacket(info, info.length, Adres, port);
							wiadomosc = new DatagramPacket(DoWyslania.getBytes(), DoWyslania.getBytes().length, odbiorca.ip, odbiorca.nr_portu);
						}
						
						aSocket.send(potwierdzenia);
						
						if (!wiadomosc.equals(null)) aSocket.send(wiadomosc);
						break;
					default:
						byte[] komunikatDefault = (polecenie + " nie ma takiego polecenia").getBytes();
						DatagramPacket packetDefault = new DatagramPacket(komunikatDefault, komunikatDefault.length, Adres, port);
						aSocket.send(packetDefault);
						break;
				}
		 	}
		 	
		} catch ( SocketException ex ) {
			Logger . getLogger ( UDPServer . class . getName ()). log( Level . SEVERE , null , ex );
		 } catch ( IOException ex) {
		 Logger . getLogger ( UDPServer . class . getName ()). log( Level . SEVERE , null , ex );
		} 
	}

	
}
