package Cw01;

import java.net.InetAddress;

public class User {
	
	    public String nazwa;
	    public InetAddress ip;
	    public int nr_portu;

	    public User(String nazwa, InetAddress ip, int port) {
	        this.nazwa = nazwa;
	        this.ip = ip;
	        this.nr_portu = port;
	    }
}
