package RMI;

import java.io.Console;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;
import java.util.Vector;

public class ChatClient {
	private IChat remoteObject;
	private String login;
	//Console consola=System.console();
	Scanner consola=new Scanner(System.in);
	public static void main(String[] args) {
		if (args.length<2) {
			System.out.println("Musisz wprowadzi� <server host> <login>");
			System.exit(-1);
		}
		
		ChatClient klient  = new ChatClient();
		klient.login = args[1];
		
		Registry registry;
		
		try {
			registry = LocateRegistry.getRegistry(args[0]);
			klient.remoteObject = (IChat) registry.lookup("ChatServer");
			
			System.out.println("Zalogowano jako: "+klient.login);
			
			while(true) {
				System.out.println();
				System.out.println("______________MENU_____________");
				System.out.println("Wybierz co chcesz zrobi�");
				System.out.println("1 - Rejestracja");
				System.out.println("2 - Wyrejestrowanie");
				System.out.println("3 - Wys�anie wiadmomosci");
				System.out.println("4 - Podaj wszystkie wiadmomo�ci ");
				System.out.println("5 - Podaj zarejestrowanych u�ytkownik�w");
				System.out.println("6 - Podaj wiadomo�ci dla danego uzytkownika");
				System.out.println("7 - Podaj wiadomosci dla danej daty");
				System.out.println("0 - Wyjdz");
				
				System.out.println("Wyb�r opcji: ");
				//String wybor = klient.consola.readLine("Wyb�r opcji: ");
				String wybor = klient.consola.nextLine();
				switch(wybor) {
				case "1":
					System.out.println("Tw�j wyb�r to: 1 - rejestracja");
					klient.rejestracja();
					break;
				case "2":
					System.out.println("Tw�j wyb�r to: 2 - Wyrejestrowanie");
					klient.wyrejestrowanie();
					break;
				case "3":
					System.out.println("Tw�j wyb�r to: 3 - Wys�anie wiadmomosci");
					klient.wiadomosc();
					break;
				case "4":
					System.out.println("Tw�j wyb�r to: 4 - Podaj wszystkie wiadmomo�ci");
					klient.getMsg();
					break;
				case "5":
					System.out.println("Tw�j wyb�r to: 5 - Podaj zarejestrowanych u�ytkownik�w");
					klient.getUser();
					break;
				case "6":
					System.out.println("Tw�j wyb�r to: 6 - Podaj wiadomo�ci dla danego uzytkownika");
					klient.getMsgByUser();
					break;
				case "7":
					System.out.println("Tw�j wyb�r to: 7 - Podaj wiadomosci dla danej daty");
					klient.getMsgByDate();
					break;
				case "0":
					System.exit(0);
				default:
					System.out.println("Wprowoadzone z�a waro��. Spr�buj jeszcze raz");
					break;
					
						
				}
				
				Thread.sleep(500);
				
			}
			
		} catch(RemoteException e) {
			e.printStackTrace();
		}catch(InterruptedException e) {
			e.printStackTrace();
		}catch(NotBoundException e) {
			e.printStackTrace();
		}
	}
	
	
	private void rejestracja() throws RemoteException{
		if(remoteObject.isUserRegistered(this.login)) {
			System.out.println("Urzytkowsnik otym loginie ju� istenie. Spr�buj jeszcze raz");
			System.exit(-1);
		}else {
			remoteObject.register(this.login);
			System.out.println("Rejestracja przebieg�a poprawnie :D. Od teraz masz login: " + this.login);
		}
	}
	
	private void wyrejestrowanie() throws RemoteException{
		boolean zarejestrowany;
		
		if(remoteObject.isUserRegistered(this.login)) {
			zarejestrowany = true;
		}else {
			System.out.println("Zaniem podejmiesz dzia�ania muszisz sie zarejestrowa�");
			zarejestrowany = false;
		}
		
		if(zarejestrowany) {
			remoteObject.unregister(this.login);
			System.out.println("Zosta�e� wyrejestrowany");
		}else return;
	}
	
	
	private void wiadomosc() throws RemoteException{
		
		if(remoteObject.isUserRegistered(this.login)) {
			String wiadomosc;
			System.out.println("Wpisz swoj� wiadmo�c: ");
			wiadomosc = consola.nextLine();
			//wiadomosc = consola.readLine("Wpisz swoj� wiadmo�c: ");
			remoteObject.sendMessage(this.login,wiadomosc);
			System.out.println("Wiadmo�� zosta�a wys�ana :D");
		}else {
			System.out.println("Zaniem podejmiesz dzia�ania muszisz sie zarejestrowa�");
			return;
		}
		
	}
	
	private void getMsg()throws RemoteException{
		if(remoteObject.isUserRegistered(this.login)) {
			Vector<Message> wiadomosci = remoteObject.getAllMessages();
			System.out.print("Obecnie wszystkich wiadmo�ci jest: "+wiadomosci.size());
			for(int i =0; i<wiadomosci.size();i++) {
				System.out.print("Wiadomo�c numer: "+ i +" - "+wiadomosci.get(i).getFormatedMessage()  );
				System.out.println("_____________________________");
			}
			System.out.println("");
		}else {
			System.out.println("Zaniem podejmiesz dzia�ania muszisz sie zarejestrowa�");
			return;
		}
	}
	
	
	private void getUser() throws RemoteException{

		if(remoteObject.isUserRegistered(this.login)) {
			Vector <String> uzytkownicy = remoteObject.getRegisteredUsers();
			System.out.print("Obecnie zarejestrowanych jest: "+uzytkownicy.size());
			for(int i =0; i<uzytkownicy.size();i++) {
				System.out.print("Urzytkownik numer: "+ i +" - "+uzytkownicy.get(i) );
				System.out.println("_____________________________");
			}
			System.out.println("");
		}else {
			System.out.println("Zaniem podejmiesz dzia�ania muszisz sie zarejestrowa�");
			return;
		}
		
	}
	
	private void getMsgByUser() throws RemoteException{
		if(remoteObject.isUserRegistered(this.login)) {
			String uzytkownik;
			System.out.println("Poni�ej wprowadz nazwe uzytkownika dla kt�rego chcesz wyswietli� wiadmomosci.");
			System.out.println("Uzytkownik: ");
			uzytkownik = consola.nextLine();
			//uzytkownik = consola.readLine("Uzytkownik: ");
			Vector<Message> wiadomosci = remoteObject.getMessagesByUser(uzytkownik);
			System.out.print("Obecnie wszystkich wiadmo�ci dla:"+ uzytkownik + "jest: " + wiadomosci.size());
			for(int i =0; i<wiadomosci.size();i++) {
				System.out.println("Wiadomo�c numer: "+ i +" - "+wiadomosci.get(i).getFormatedMessage() );
				System.out.println("_____________________________");
			}
			System.out.println("");
		}else {
			System.out.println("Zaniem podejmiesz dzia�ania muszisz sie zarejestrowa�");
			return;
		}
	}
	
	private void getMsgByDate() throws RemoteException{
		if(remoteObject.isUserRegistered(this.login)) {
			String data;
			System.out.println("Poni�ej wprowadz date dla kt�rego chcesz wyswietli� wiadmomosci. Format: yyyy-mm-dd hh:mm:ss");
			System.out.println("Data: ");
			data = consola.nextLine();
			//data = consola.readLine("Uzytkownik: ");
			Vector<Message> wiadomosci = remoteObject.getMessagesByDate(data);
			System.out.print("Obecnie wszystkich wiadmo�ci z:"+ data + "jest: " + wiadomosci.size());
			for(int i =0; i<wiadomosci.size();i++) {
				System.out.println("Wiadomo�c numer: "+ i +" - "+wiadomosci.get(i).getFormatedMessage() );
				System.out.println("_____________________________");
			}
			System.out.println("");
		}else {
			System.out.println("Zaniem podejmiesz dzia�ania muszisz sie zarejestrowa�");
			return;
		}
	}

}
