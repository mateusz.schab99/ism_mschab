package RMI;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Vector;

public class ChatServant extends UnicastRemoteObject implements IChat{
	
	private Vector<String> Users;
	private Vector <Message> Message;
	
	
	protected ChatServant() throws RemoteException {
		Users = new Vector<String>();
		Message = new Vector<Message>();
	}
	
	//U�ytkownicy
	public Vector<String> getRegisteredUsers() throws RemoteException {
		return this.Users;
	}

	//Wiadomosci
	public Vector<Message> getAllMessages() throws RemoteException {
		return this.Message;
		
	}
	//zarejestrownaie
	public boolean register(String nick) throws RemoteException {
		System.out.println("U�ytkownik o nicku: "+ nick +"zosta� zarejestrowany :D");
		return this.Users.add(nick);
	}

	  public boolean isUserRegistered(String nick) throws RemoteException {
	        return Users.contains(nick);
	   }
	  
	  //wyrejstrownia
	  public boolean unregister(String nick) throws RemoteException {
	        System.out.println("U�ytkownik o nicku: " + nick + " zosta� wyrejestrowany");
	        return this.Users.remove(nick);
	    }
	  
	  //Wysy�anie wiadmomosci
	    public void sendMessage(String msg, String user) throws RemoteException {
	        Message Msg = new Message(user, msg);
	        this.Message.add(Msg);
	        System.out.println(Msg.getFormatedMessage());
	    }
	    
	    public Vector<Message> getMessagesByUser(String user) {
	        Vector<Message> filteredMessages = new Vector<Message>();

	        for (int i = 0; i < this.Message.size(); i++) {
	            if (this.Message.get(i).getUser().equals(user)) {
	                filteredMessages.add(this.Message.get(i));
	            }
	        }

	        return filteredMessages;
	    }

	    public Vector<Message> getMessagesByDate(String date) {
	        Vector<Message> filteredMessages = new Vector<Message>();
	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	        LocalDateTime convertedDate = LocalDateTime.parse(date, formatter);

	        for (int i = 0; i < this.Message.size(); i++) {
	            if (this.Message.get(i).getDate().isAfter(convertedDate)) {
	                filteredMessages.add(this.Message.get(i));
	            }
	        }

	        return filteredMessages;
	    }

	
	    
}
