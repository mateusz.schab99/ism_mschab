package RMI;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ChatServer {
	ChatServant servant;
	Registry registry;
	
	public static void main(String[] args) {
		try {
			ChatServer server = new ChatServer();
		
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	protected ChatServer() throws RemoteException{
		try {
			registry = LocateRegistry.createRegistry(1099);
			servant = new ChatServant();
			registry.rebind("ChatServer", servant);
            System.out.println("Server ready");
		}catch (RemoteException e) {
            e.printStackTrace();
            throw e;
        }
	}
}
