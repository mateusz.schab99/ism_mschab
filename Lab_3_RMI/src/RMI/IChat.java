package RMI;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Vector;

public interface IChat extends Remote {

	boolean unregister(String nick) throws RemoteException;
    boolean register(String nick) throws RemoteException;
    boolean isUserRegistered(String nick) throws RemoteException;

    void sendMessage(String msg, String user) throws RemoteException;

    Vector<String> getRegisteredUsers() throws RemoteException;

    Vector<Message> getAllMessages() throws RemoteException;
    Vector<Message> getMessagesByUser(String user) throws RemoteException;
    Vector<Message> getMessagesByDate(String date) throws RemoteException;
}
