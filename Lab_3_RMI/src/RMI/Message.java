package RMI;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class Message implements Serializable {
	private String uzytkownik;
    private String wiadomosc;
    private LocalDateTime data;

    public Message(String uzytkownik, String wiadomosc) {
        this.uzytkownik = uzytkownik;
        this.wiadomosc = wiadomosc;
        this.data = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);
    }

  

	public String getUser() {
        return uzytkownik;
    }

    public String getText() {
        return wiadomosc;
    }

    public LocalDateTime getDate() {
        return data;
    }

    public String getFormatedMessage() {
        return data + "  " + uzytkownik + ":  " + wiadomosc;
    }

}
