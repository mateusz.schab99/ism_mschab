package agh.demo.course;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController()
public class BooksController {
	//Kontroler jest takim pośrednikiem który rozmawia z klientem i wydaje polecenia serwisom
	//Stąd skrót API - Application Programming Inteface
	//Jest to warstwa udostępniona klientowi tak żeby nie mógł grzebać we wnętrzu bo na pewno coś popsuje
	//Działa to w ten sposób, że wysyłamy żądanie na dany adres URL
	//Na początku jest nazwa domeny - u nas lokalnie więc localhost:8080
	//Po sleszu jakieś konkrety - troche jak struktura folderów
	//API - jeżeli o coś pytamy - zwraca dane najczęściej w formacie JSON 
	//czyli tablica (lista) obiektów przekonwertowana na jednego dużego stringa tak żeby można było to przesłać 
	//Jeżeli każemy coś zrobić kod - czyli status jak zakończyła się operacja
	//Protokół http ma liste zdefiniowanych kodów - te które zaczynaja się od 2 to sukces (np. 200 - OK),
	//od 4 - błąd klienta (404 - nie znaleziono, 401 - brak autoryzacji), od 5 - błąd serwera (500 - błąd w logice który przerwał dziłanie apki) itd.
	//Tutaj nie wpisujemy ręcznie tych kodów tylko korzytamy z enuma HttpStatus
	
	@Autowired()
	private BooksService service; //Serwis który obsługuje logike
	
	//Jeżeli po localhost będzie doklejone "books" wywołujemy ta metode
	//metoda GET - zwraca coś
	@RequestMapping("/books")
	public List<Book> getBooks() {
		return service.getBooks();
	}
	
	//Jeżeli po 'courses' będzie jakis numer to metode do znalezenia konkretnrj książki
	//Id (ISBN) jest w nawiasach bo to tylko symbol - w zależności od książki będzie sie różniło
	//Dekorator @PathVariable mówi że id ma pobrać z adresu z miejsca w nawiasach
	//Obsuła wyjątku - dla reszty jest w serwisie - tam ci to opisałem 
	//Jeżeli znalazło ksiązke zwracamy ją, jeżeli nie wysyłamy jako odpowiedz status NOT FOUND
	@RequestMapping("/books/{ISBN}")
	public Book getBook(@PathVariable int ISBN) {
		try {
			return service.getBook(ISBN);
		} catch (NoSuchElementException ex) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}
	
	//metoda PUT - służy do dodawania zasobu
	//Dekorator @RequestBody oznacza że razem z zapytaniem na serwer musi być wysłany obiekt klasy książka
	//W zależności od rezultatu zwracamy odpowiedni kod
	@PutMapping(value = "/books")
	public void addCourse (@RequestBody Book book) {
		boolean result = service.addBook(book);
		
		if (result)
			throw new ResponseStatusException(HttpStatus.CREATED);
		else 
			throw new ResponseStatusException(HttpStatus.CONFLICT);
	}
	
	//metoda DELETE - usuwanie
	//resta podobnie jak poprzednio
	@DeleteMapping(value = "/books/{ISBN}")
	public void deleteCourse(@PathVariable int ISBN) {
		boolean result = service.deleteBook(ISBN);
		
		if (result) 
			throw new ResponseStatusException(HttpStatus.OK);
		else 
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
	}
	
	@PostMapping(value = "/books/{id}")
	public void editCourse(@PathVariable int ISBN, @RequestBody Book editedBook) {
		boolean result = service.editBook(ISBN, editedBook);
		
		if (result)
			throw new ResponseStatusException(HttpStatus.OK);
		else 
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
	}
}
