package agh.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {
	//Klasa stowrzona na podstawie tutoriala

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
