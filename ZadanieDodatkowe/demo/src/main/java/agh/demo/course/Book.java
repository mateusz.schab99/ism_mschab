package agh.demo.course;

import java.util.List;

public class Book {
	//Klasa opisująca model - książka. Pola opisujące własności, konstruktor, funkcje get i set
	
	private int ISBN;
	private String tytul;
	private String autor;
	private String opis;
	
	//Klas Book z atrybutami takimi jak IBSN autor tyuł opis
	public Book(int ISBN, String tytul, String autor, String opis) {
		this.ISBN = ISBN;
		this.tytul = tytul;
		this.autor = autor;
		this.opis = opis;
	}

	public int getISBN() {
		return ISBN;
	}

	public void setISBN(int isbn) {
		ISBN = isbn;
	}

	public String getTitle() {
		return tytul;
	}

	public void setTitle(String tytul) {
		this.tytul = tytul;
	}

	public String getAuthor() {
		return autor;
	}

	public void setAuthor(String autor) {
		this.autor = autor;
	}

	public String getDescription() {
		return opis;
	}

	public void setDescription(String opis) {
		this.opis = opis;
	}
	

}
