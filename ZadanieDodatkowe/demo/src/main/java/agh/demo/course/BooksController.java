package agh.demo.course;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController()
public class BooksController {

	
	//Serwis który obsługujący logike
	@Autowired()
	private BooksService serwis;
	
	@RequestMapping("/books")
	public List<Book> getBooks() {
		return serwis.getBooks();
	}
	
	//metoda do usuwania
	@DeleteMapping(value = "/books/{ISBN}")
	public void deleteCourse(@PathVariable int ISBN) {
		boolean result = serwis.deleteBook(ISBN);
		
		if (result) 
			throw new ResponseStatusException(HttpStatus.OK);
		else 
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
	}
	
	//metoda do szukania konkretnej książki opisanej IBSN
	//PathVariable daje inforamcje o tym ze id (ISBN) jest pobierany z tego co w nawiaskach
	@RequestMapping("/books/{ISBN}")
	public Book getBook(@PathVariable int ISBN) {
		try {
			return serwis.getBook(ISBN);
		} catch (NoSuchElementException ex) { //obsługa wyjatku
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}
	
	// dodawania zasobów (metoda PUT) wymaga wysłania na server obiektu typu książka
	@PutMapping(value = "/books")
	public void addBook (@RequestBody Book book) {
		boolean result = serwis.addBook(book);
		
		if (result)
			throw new ResponseStatusException(HttpStatus.CREATED);
		else 
			throw new ResponseStatusException(HttpStatus.CONFLICT);
	}
	
	
	//metoda do edytowania wymaga ID i obiekt zyli książke - metoda Post
	@PostMapping(value = "/books/{ISBN}")
	public void editBook(@PathVariable int ISBN, @RequestBody Book editedBook) {
		boolean result = serwis.editBook(ISBN, editedBook);
		
		if (result)
			throw new ResponseStatusException(HttpStatus.OK);
		else 
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
	}
}
