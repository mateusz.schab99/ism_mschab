package agh.demo.course;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.stereotype.Service;

@Service()
public class BooksService {
	//Dodanie książek żeby w lokalnej bazie coś widniało
	private List<Book> ksiazki = new ArrayList(Arrays.asList(
			new Book(11, "Bogaty ojciec. Biedny ojciec.", "Kiyosaki Robert", "Ponadczasowy poradnik dotyczący finansów osobistych"),
			new Book(22, "FINANSOWA FORTECA", "Marcin Iwuć", "Finansowa Forteca to praktyczny przewodnik, który wyprowadzi Cię z inwestycyjnych krzaków i da zestaw umiejętności potrzebnych do tego, żeby rozpocząć swoją przygodę z inwestowaniem na poważnie."),
			new Book(33, "Serwonapędy SIEMENS w praktyce inżynierskiej", "Janusz Kwaśniewski, Ireneusz Dominik, Krzysztof Lalik, Mateusz Kozek", "Książka w sposób metodyczny i przyjazny opisuje projektowanie serwonapędów bazujących na sterownikach S7-1500, napędach SINAMICS i oprogramowaniu TIA Portal 15.1."),
			new Book(44, "Lalka ", "Bolesław Prus", "Miałem na maturze :/")
			));
	
	
	//Dodajemy do listy nowy obiekt (Książka)
	//Pod warunkiem że takiej ksiazki o takim ID  już niema
	//z zalożenia id ma byc unikalne
	public boolean addBook(Book book) {
		try {
			getBook(book.getISBN());
		} catch(NoSuchElementException ex) {
			this.ksiazki.add(book);
			return true;
		}
		
		return false;
		}
		
	//Wyświetlenie całej listy książek 
	public List<Book> getBooks() {
		return this.ksiazki;
	}
	
	//Edytowanie atrybutów ksiazki 
	public boolean editBook(int id, Book edited) {
		Book bookToEdit;
		try {
			bookToEdit = getBook(id);
		} catch (NoSuchElementException ex) {
			return false;
		}
		
		bookToEdit.setISBN(edited.getISBN());
		bookToEdit.setTitle(edited.getTitle());
		bookToEdit.setAuthor(edited.getAuthor());
		bookToEdit.setDescription(edited.getDescription());
		return true;
	}	
	
	//Wyszukiwanie ksiązki po id  (ISBN)
	public Book getBook(int id) {
		return this.ksiazki.stream().filter(c -> c.getISBN() == id).findFirst().get();
	}
	
	//Usuwanie kisążki o konkretnym id 
	public boolean deleteBook(int id) {
		try {
			getBook(id);
		} catch(NoSuchElementException ex) {
			return false;
		}
		//jezeli jest taka ksiazka o danym ID to zostanie usunięta
		this.ksiazki = this.ksiazki.stream().filter(c -> !(c.getISBN() == id)).toList();
		return true;
	}
	
}

