package agh.demo;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.springframework.http.MediaType;

import agh.demo.course.Book;

class TestClient {
	//Program który symuluje wysyłanie żądań na serwer 
	private final String Localhost = "http://localhost";
	private final short portNumber = 8080;
	private final String booksUrl = "books";
	
	private final Client klient = ClientBuilder.newClient();
	private final WebTarget baseTarget = klient.target(Localhost + ":" + portNumber);
	private final WebTarget booksTarget = baseTarget.path(booksUrl);
	private final WebTarget singleBookTarget = booksTarget.path("{id}");
	
	public static void main (String[] args) {
		TestClient testClient = new TestClient();
		
		//wyświetlenie wszytsekgo 
		Response response = testClient.get();
		System.out.println("GET ALL");
		System.out.println(response);
		List<Book> books = (List<Book>) response.readEntity(Book.class);
		System.out.println(books);
		
		//wyswietlenie po ID
		response = testClient.get(123);
		System.out.println("GET WITH ID");
		System.out.println(response);
		Book course = (Book) response.readEntity(Book.class);
		System.out.println(course);
		
		//Dodanie 
		Book newCourse = new Book(258, "Tytuł", "Autor", "Opis");
		response = testClient.put(newCourse);
		System.out.println("PUT");
		System.out.println(response);
		
		//Edytowanie
		Book updateCourse = new Book(123, "Tytuł", "Autor", "Opis");
		response = testClient.post(updateCourse.getISBN(), updateCourse);
		System.out.println("POST");
		System.out.println(response);
		
		//Usunięcie
		int idToDelete = 456;
		response = testClient.delete(idToDelete);
		System.out.println("DELETE");
		System.out.println(response);
		
	}
	
	private Response get() {
		Invocation.Builder invocationBuilder = booksTarget.request();
		return invocationBuilder.get();
	}
	
	private Response delete(int id) {
		return booksTarget.resolveTemplate("id", id).request().delete();
	}
	
	private Response get(int id) {
		WebTarget target = singleBookTarget.resolveTemplate("id", id);
		Invocation.Builder invocationBuilder = target.request();
		return invocationBuilder.get();
	}
	private Response post(int id, Book book) {
		return booksTarget.resolveTemplate("id", id).request().post(Entity.json(book));
	}
	
	private Response put(Book book) {
		return booksTarget.request().put(Entity.json(book));
	}
	
}

